package main

import (
	"github.com/gin-gonic/gin"
	"sellerfoxStatisticsPkg"
	"github.com/gin-gonic/contrib/static"
)

func main() {
	r := gin.Default() //gin.New()
	r.LoadHTMLTemplates("form.html")
	r.LoadHTMLTemplates("chart.html")
	r.LoadHTMLTemplates("stats_galleries.html")
	r.LoadHTMLTemplates("stats_clients.html")
	r.Use(static.Serve("/", static.LocalFile("static", false)))

	//http://localhost:8082/chart
	r.GET("/chart", func(c *gin.Context) {
			//obj := gin.H{"title": "Main form"}
			//draw html form
			c.HTML(200, "chart.html", "")
			sellerfoxStatisticsPkg.Simple_select()
	})
	//http://localhost:8082/chart_new
	r.GET("/chart_new", func(c *gin.Context) {
		//obj := gin.H{"title": "Main form"}
		//draw html form
		c.HTML(200, "chart.html", "")
		sellerfoxStatisticsPkg.Simple_select_new()
	})
	//http://localhost:8082/form
	r.GET("/form", func(c *gin.Context) {
		//obj := gin.H{"title": "Main form"}
		//draw html form
		c.HTML(200, "form.html", "")
	})
	//http://localhost:8082/form_update_clients
	r.POST("/form_update_clients", func(c *gin.Context) {
		//c.String(200, "Script started working!\n")
		c.HTML(200, "form.html", "")
		databases_array := sellerfoxStatisticsPkg.Show_databases()
		sellerfoxStatisticsPkg.Get_clients(databases_array, c)
		c.String(200, "&nbsp;&nbsp;&nbsp;Script finished working!\n")
	})
	//http://localhost:8082/form_update_galleries
	r.POST("/form_update_galleries", func(c *gin.Context) {
		//c.String(200, "Script started working!\n")
		c.HTML(200, "form.html", "")
		databases_array := sellerfoxStatisticsPkg.Show_databases()
		sellerfoxStatisticsPkg.Get_galleries(databases_array, c)
		c.String(200, "&nbsp;&nbsp;&nbsp;Script finished working!\n")
	})
	//http://localhost:8082/form_update_contracts
	r.POST("/form_update_contracts", func(c *gin.Context) {
		//c.String(200, "Script started working!\n")
		c.HTML(200, "form.html", "")
		databases_array := sellerfoxStatisticsPkg.Show_databases()
		sellerfoxStatisticsPkg.Get_contracts(databases_array, c)
		c.String(200, "&nbsp;&nbsp;&nbsp;Script finished working!\n")
	})
	//http://localhost:8082/form_queries_galleries
	r.POST("/form_queries_galleries", func(c *gin.Context) {
		//c.HTML(200, "form_queries_galleries.html", "")
		gallery_total, gallery_type, gallery_lang, gallery_auct, gallery_client, gallery_wh, gallery_names := sellerfoxStatisticsPkg.Get_stats_galleries()
		obj := gin.H{	"gallery_total": 	gallery_total,
						"gallery_type": 	gallery_type,
						"gallery_lang": 	gallery_lang,
						"gallery_auct": 	gallery_auct,
						"gallery_client": 	gallery_client,
						"gallery_wh": 		gallery_wh,
						"gallery_names": 	gallery_names,
		}
		c.HTML(200, "stats_galleries.html", obj)
	})
	//http://localhost:8082/form_queries_clients
	r.POST("/form_queries_clients", func(c *gin.Context) {
		//c.HTML(200, "form_queries_galleries.html", "")
		total_clients, client_land, client_cancelled, client_blocked, client_price, client_title := sellerfoxStatisticsPkg.Get_stats_clients()
		obj := gin.H{   "total_clients":    total_clients,
						"client_land":    	client_land,
						"client_cancelled": client_cancelled,
						"client_blocked": 	client_blocked,
						"client_price": 	client_price,
						"client_title": 	client_title,
		}
		c.HTML(200, "stats_clients.html", obj)
		})
	// Listen and server on 0.0.0.0:8082
	r.Run(":8082")
}
