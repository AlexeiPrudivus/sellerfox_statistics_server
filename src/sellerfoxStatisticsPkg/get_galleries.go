package sellerfoxStatisticsPkg

import (
	"github.com/gin-gonic/gin"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"time"
	"strconv"
	"strings"
	"github.com/jmoiron/sqlx"
)

type R_gallery struct {
	Id, Name, Language, Show_categories, Height, Width, Auction_count string
}

func Get_galleries(databases_array []string, c *gin.Context) () {
	//set start time
	t := time.Now().Local()
	start_time := string(t.Format("15:04:05"))
	fmt.Println("start_time:", start_time)

	progress_full := len(databases_array)
	fmt.Println("progress_full:", progress_full)
	progress_current := 1
//================================================ connect to local database ================================================//
	//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")

	db2, err := sqlx.Connect("mysql", "root:root-sellerfox@tcp(localhost:3306)/sellerfox_statistics?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect to sellerfox statistics.", err)
		return
	} else {
		fmt.Println("Connected to db sellerfox statistics.")
	}
	defer db2.Close()
//========================================================== cycle! =========================================================//
	for _, sf_db := range databases_array {
//================================================= get client's sf_login_id ================================================//
	sf_login_id := strings.Replace(sf_db, "sellerfox_", "", -1)
//=========================================== connect to sellerfox_xxxxx database ===========================================//
		//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
		db1, err := sqlx.Connect("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/"+sf_db+"?charset=utf8")
		if err != nil {
			fmt.Println("Failed to connect to ",sf_db,". ", err)
			return
		} else {
			fmt.Println("Connected to db "+sf_db+".")
		}
//============================================= get client's gallery information ============================================//
		tx1 := db1.MustBegin()
		//execute query
		rows_gallery_info := []R_gallery{}
		var e error
		e = tx1.Select(&rows_gallery_info,"SELECT sfg_gallery.id, sfg_gallery.name, sfg_gallery.language, sfg_gallery.show_categories, sfg_gallery.height, sfg_gallery.width, sfg_gallery.auction_count FROM sfg_gallery")
		tx1.Commit()
		if e != nil {
			fmt.Println("Could not get client's gallery information. Client " + sf_db + ".")
			fmt.Println(e)
		}
//========================================================== cycle! =========================================================//
		for _, row_gallery := range rows_gallery_info {
			fmt.Println(sf_login_id+"_"+row_gallery.Id + ", '" + sf_login_id + "', '" + row_gallery.Name + "', '" + row_gallery.Language + "', '" + row_gallery.Show_categories + "', " + row_gallery.Height + ", " + row_gallery.Width + ", " + row_gallery.Auction_count)
			//escape characters
			row_gallery.Name = strings.Replace(row_gallery.Name, "'", "&#39;", -1)  // '
			row_gallery.Name = strings.Replace(row_gallery.Name, "\"", "&#34;", -1) // "
//========================================== copy gallery info into local database ==========================================//
			tx2 := db2.MustBegin()
			//execute query
			_, err := tx2.Exec("INSERT INTO gallery (gallery_id, sf_login_id, name, language, show_categories, height, width, auction_count) " +
						//Gallery_id, Sf_login_id, Name, Language, Show_categories, Height, Width, Auction_count
								"VALUES ('" + sf_login_id+"_"+row_gallery.Id + "', '" + sf_login_id + "', '" + row_gallery.Name + "', '" + row_gallery.Language + "', '" + row_gallery.Show_categories + "', " + row_gallery.Height + ", " + row_gallery.Width + ", " + row_gallery.Auction_count + ") " +
								"ON DUPLICATE KEY UPDATE " +
								"name = '" + row_gallery.Name + "', " +
								"language = '" + row_gallery.Language + "', " +
								"show_categories = " + row_gallery.Show_categories + ", " +
								"height = " + row_gallery.Height + ", " +
								"width = " + row_gallery.Width + ", " +
								"auction_count = " + row_gallery.Auction_count)
			tx2.Commit()
			if err != nil {
				fmt.Println("Could not get gallery information. Client " + sf_db + ".")
				fmt.Println(err)
			}
		}
		db1.Close() //close sellerfox_xxxxx db connection
//================================================== real-time progress bar =================================================//
		var progress_percent float32 = float32(progress_current) / float32(progress_full)* 100
		if progress_current == 1 || progress_current % (progress_full / 200) == 0 || progress_current == progress_full{
			progress_string	:= "<div id='script'>" +
					"<script>" +
					"document.getElementById('progress').remove();" +
					"document.getElementById('script').remove();" +
					"document.getElementById('filler').remove();" +
					"</script>" +
					"</div>" +
					"<div class='progress' style='margin: 0.5%;' id='progress'>" +
					"<div class='progress-bar' role='progressbar' aria-valuenow='"+strconv.Itoa(progress_current)+"' aria-valuemin='0' aria-valuemax='"+strconv.Itoa(progress_full)+"' style='width: "+strconv.FormatFloat(float64(progress_percent), 'f', 1, 32)+"%;'>" +
					"<span class='sr-only'>text</span>" +
					"</div>" +
					"</div>\n"
			dummy_symbols_count := 4096 - len(progress_string)
			dummy_string := "<div id='filler'>"
			for i :=0; i < dummy_symbols_count/20 - 23; i++ {
				dummy_string += "<!--/n/n/n/n/n/n/n/n/n/n/n/n/n/n/n/n/-->"
			}
			dummy_string += "</div>"
			c.String(200, progress_string+dummy_string)
		}
		progress_current++
	}
//======================================================== cycle over =======================================================//
	//set end time
	t = time.Now().Local()
	end_time := string(t.Format("15:04:05"))
	fmt.Println("\nend_time:", end_time)
}
