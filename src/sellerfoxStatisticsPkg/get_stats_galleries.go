package sellerfoxStatisticsPkg

import (
	"github.com/jmoiron/sqlx"
	"fmt"
)

type Gallery_total_query struct {
	Total string
}

type Gallery_type_query struct {
	Count_show_categories string
	Show_categories string
}

type Gallery_lang_query struct {
	Count_lang string
	Language string
}

type Gallery_auct_query struct {
	Count_auct string
	Auction_count string
}

type Gallery_client_query struct {
	Count_client string
	Count_gallery string
}

type Gallery_wh_query struct {
	Gallery_width string
	Gallery_height string
}

func Get_stats_galleries()(string, [][]string, [][]string, [][]string, [][]string, [][]string, string) {
//================================================ connect to local database ================================================//
	db, err := sqlx.Connect("mysql", "root:root-sellerfox@tcp(localhost:3306)/sellerfox_statistics?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect to sellerfox statistics.", err)
	} else {
		//fmt.Println("Connected to db sellerfox statistics.") //temp
	}
	defer db.Close()
//=============================================== get number of all galleries ===============================================//
	row_gallery_count := []Gallery_total_query{}
	tx := db.MustBegin()
	var e error
	e = tx.Select(&row_gallery_count,"SELECT count(gallery.show_categories)as total FROM gallery")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get total amount of galleries.")
		fmt.Println(e)
	}
	//fmt.Println("Total number of galleries:",row_gallery_count[0].Total) //delete later
//============================================ get number of galleries by types =============================================//
	row_gallery_type := []Gallery_type_query{}
	tx = db.MustBegin()
	e = tx.Select(&row_gallery_type,"SELECT count(show_categories) as count_show_categories, show_categories FROM `gallery` GROUP BY show_categories ORDER BY count_show_categories DESC")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get types galleries.")
		fmt.Println(e)
	}
	//fmt.Println("Total number of type 0 galleries:",row_gallery_0_count[0].Total) //delete later
//================================== pack query results about gallery types into an array ===================================//
	//create a new 2-dimensional array to output data
	data_array_type := make([][]string, len(row_gallery_type))
	for i := range data_array_type {
		data_array_type[i] = make([]string, 2)
	}
	//fill new array with data received from query
	for i := 0; i < len(row_gallery_type); i++ {
		switch row_gallery_type[i].Show_categories {
			//Display Shop Category Carousel Coverflow Polaroid Wall
			case "0": {
				row_gallery_type[i].Show_categories = "Display"
			}
			case "1": {
				row_gallery_type[i].Show_categories = "Shop"
			}
			case "2": {
				row_gallery_type[i].Show_categories = "Category"
			}
			case "3": {
				row_gallery_type[i].Show_categories = "Carousel"
			}
			case "4": {
				row_gallery_type[i].Show_categories = "Coverflow"
			}
			case "5": {
				row_gallery_type[i].Show_categories = "Polaroid"
			}
			case "6": {
				row_gallery_type[i].Show_categories = "Wall"
			}
		}
		data_array_type[i][0] = row_gallery_type[i].Count_show_categories
		data_array_type[i][1] = row_gallery_type[i].Show_categories
	}
	//fmt.Println(data_array_lang) //delete later
//====================================== get number of galleries in a certain language ======================================//
	row_gallery_lang := []Gallery_lang_query{}
	tx = db.MustBegin()
	e = tx.Select(&row_gallery_lang,"SELECT count(language) as count_lang, language FROM `gallery` GROUP BY language ORDER BY count_lang DESC")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get gallery languages.")
		fmt.Println(e)
	}
//===============================- pack query results about gallery languages into an array -================================//
	//create a new 2-dimensional array to output data
	data_array_lang := make([][]string, len(row_gallery_lang))
	for i := range data_array_lang {
		data_array_lang[i] = make([]string, 2)
	}
	//fill new array with data received from query
	for i := 0; i < len(row_gallery_lang); i++ {
		if row_gallery_lang[i].Language == "" {
			row_gallery_lang[i].Language = "--"
		}
		data_array_lang[i][0] = row_gallery_lang[i].Count_lang
		data_array_lang[i][1] = row_gallery_lang[i].Language
	}
	//fmt.Println(data_array_lang) //delete later
//========================================= get number of auctions per gallery page=== ======================================//
	row_gallery_auct := []Gallery_auct_query{}
	tx = db.MustBegin()
	e = tx.Select(&row_gallery_auct,"SELECT count(auction_count) as count_auct, auction_count FROM `gallery` group by auction_count ORDER BY count_auct DESC")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get total amount of galleries.")
		fmt.Println(e)
	}
//=========================== pack query results about number of auctions per page into an array ============================//
	//create a new 2-dimensional array to output data
	data_array_auct := make([][]string, len(row_gallery_auct))
	for i := range data_array_auct {
		data_array_auct[i] = make([]string, 2)
	}
	//fill new array with data received from query
	for i := 0; i < len(row_gallery_auct); i++ {
		data_array_auct[i][0] = row_gallery_auct[i].Count_auct
		data_array_auct[i][1] = row_gallery_auct[i].Auction_count
	}
	//fmt.Println(data_array_auct) //delete later
//============================================ get number of galleries per user =============================================//
	row_gallery_client := []Gallery_client_query{}
	tx = db.MustBegin()
	e = tx.Select(&row_gallery_client,"SELECT count(count_gallery) as count_client, count_gallery  from (SELECT count(sf_login_id) as count_gallery FROM `gallery` GROUP BY sf_login_id) as t1 group by count_gallery ORDER BY count_client DESC")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get total amount of galleries.")
		fmt.Println(e)
	}
	//fmt.Println(row_gallery_client) //delete later
//=========================== pack query results about number of galleries per user into an array ===========================//
	//create a new 2-dimensional array to output data
	data_array_client := make([][]string, len(row_gallery_client))
	for i := range data_array_client {
		data_array_client[i] = make([]string, 2)
	}
	//fill new array with data received from query
	for i := 0; i < len(row_gallery_client); i++ {
		data_array_client[i][0] = row_gallery_client[i].Count_client
		data_array_client[i][1] = row_gallery_client[i].Count_gallery
	}
	//fmt.Println(data_array_auct) //delete later
//=============================== get custom gallery dimensions without defaults and extremes ===============================//
	//exclude default values and extreme values (>200px and <1000px)
	//500 x 300
	//960 x 830
	row_gallery_wh := []Gallery_wh_query{}
	tx = db.MustBegin()
	e = tx.Select(&row_gallery_wh,"SELECT ROUND(AVG(width), 0) as gallery_width, ROUND(AVG(height), 0) as gallery_height FROM gallery WHERE width <> 300 AND width <> 830 AND width > 200 AND width < 1000 AND height <> 500 AND height <> 960 AND height > 200 AND height < 1000")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get custom dimensions of galleries 1.")
		fmt.Println(e)
	}
	//fmt.Println(row_gallery_wh) //delete later
//================================ pack query results about gallery dimensions into an array ================================//
	//create a new 2-dimensional array to output data
	data_array_wh := make([][]string, 3)
	for i := range data_array_wh {
		data_array_wh[i] = make([]string, 2)
	}
	//fill new array with data received from query
	data_array_wh[0][0] = row_gallery_wh[0].Gallery_width
	data_array_wh[0][1] = row_gallery_wh[0].Gallery_height
	//fmt.Println(data_array_wh) //delete later
//====================================== get custom gallery dimensions without defaults =====================================//
	//exclude default values
	//500 x 300
	//960 x 830
	row_gallery_wh = []Gallery_wh_query{}
	tx = db.MustBegin()
	e = tx.Select(&row_gallery_wh,"SELECT ROUND(AVG(width), 0) as gallery_width, ROUND(AVG(height), 0) as gallery_height FROM gallery WHERE width <> 300 AND width <> 830 AND height <> 500 AND height <> 960")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get custom dimensions of galleries 2.")
		fmt.Println(e)
	}
	//fmt.Println(row_gallery_wh) //delete later
//================================ pack query results about gallery dimensions into an array ================================//
	//fill new array with data received from query
	data_array_wh[1][0] = row_gallery_wh[0].Gallery_width
	data_array_wh[1][1] = row_gallery_wh[0].Gallery_height
	//fmt.Println(data_array_wh) //delete later
//================================================ get all gallery dimensions ===============================================//
	//500 x 300
	//960 x 830
	row_gallery_wh = []Gallery_wh_query{}
	tx = db.MustBegin()
	e = tx.Select(&row_gallery_wh,"SELECT ROUND(AVG(width), 0) as gallery_width, ROUND(AVG(height), 0) as gallery_height FROM gallery")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get custom dimensions of galleries 3.")
		fmt.Println(e)
	}
	//fmt.Println(row_gallery_wh) //delete later
//================================ pack query results about gallery dimensions into an array ================================//
	//fill new array with data received from query
	data_array_wh[2][0] = row_gallery_wh[0].Gallery_width
	data_array_wh[2][1] = row_gallery_wh[0].Gallery_height
	//fmt.Println(data_array_wh) //delete later
//================================================ get standard gallery names ===============================================//
	row_gallery_name := []Client_cancelled_query{}
	tx = db.MustBegin()
	e = tx.Select(&row_gallery_name,"	SELECT count(name) as total FROM (SELECT name FROM gallery WHERE "+
										"name LIKE 'DisplayGallery-%' "+
										"OR name LIKE 'ShopGallery-%' "+
										"OR name LIKE 'CategoryGallery-%' "+
										"OR name LIKE 'CarrouselGallery-%' "+
										"OR name LIKE 'CoverflowGallery-%' "+
										"OR name LIKE 'PolaroidGallery-%' "+
										"OR name LIKE 'WallGallery-%' "+
										") as t1")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get standard names of galleries.")
		fmt.Println(e)
	}
	//fmt.Println(row_gallery_wh) //delete later
//================================== pack query results about gallery names into an array ===================================//
	//fill new array with data received from query
	data_gallery_names := row_gallery_name[0].Total

	return row_gallery_count[0].Total, data_array_type, data_array_lang, data_array_auct, data_array_client, data_array_wh, data_gallery_names
}
