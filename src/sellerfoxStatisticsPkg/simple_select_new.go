package sellerfoxStatisticsPkg

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type Titles struct {
	Title string
}

func Simple_select_new() {
//============================================== connect to sellerfox database ==============================================//
	//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
	//"mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/sellerfox?charset=utf8"
	//statistics:statisticspass
	db, err := sqlx.Connect("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/sellerfox?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect to sellerfox.", err)
	} else {
		fmt.Println("Connected to db sellerfox.")
	}
	defer db.Close()
//================================================= get a list of databases =================================================//
	tx := db.MustBegin()
	//execute query
	rows := []Titles{}
	var e error
	e = tx.Select(&rows,"SELECT DISTINCT title as title FROM sf_tariff")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get titles.")
		fmt.Println(e)
	}

	fmt.Println("List of titles:",rows)
	//fmt.Println(databases_array)
}
