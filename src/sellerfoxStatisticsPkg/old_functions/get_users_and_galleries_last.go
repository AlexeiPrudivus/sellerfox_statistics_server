package sellerfoxStatisticsPkg

import (
	"github.com/gin-gonic/gin"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
//	"log"
	"time"
	"strconv"
)

func Get_users_and_galleries(databases_array []string, c *gin.Context)() {
	//set start time
	t := time.Now().Local()
	start_time := string(t.Format("15:04:05"))
	fmt.Println("start_time:",start_time)
	c.String(200, "start_time: "+start_time+"\n")

	progress_full := len(databases_array)
	fmt.Println("progress_full:",progress_full)
	c.String(200, strconv.Itoa(progress_full)+"\n")
	progress_current := 0
//============================================== connect to sellerfox database ==============================================//
	//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
	db, err := sql.Open("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/sellerfox?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect", err)
		return
	} else {
		fmt.Println("Connected to db sellerfox.")
	}
	defer db.Close()
//=========================================== if database is found, connect to it ===========================================//
	for _, sf_db := range databases_array {
		//connect to DB
		//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
		db, err := sql.Open("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/"+sf_db+"?charset=utf8") //"+sf_login_id+"
		if err != nil {
			fmt.Println("Failed to connect", err)
		} else {
			//fmt.Println("\nConnected to db " + sf_db + ".")
			//c.String(200, "\nConnected to db " + sf_db + ".\n")
		}
		defer db.Close()
//============================================== check if required table exists =============================================//
		//execute query
		rows_table_like, err := db.Query("SHOW TABLES LIKE 'sfg_gallery'")
		if err != nil {
			fmt.Println("Could not check if required tables exist user db " + sf_db + ".")
			fmt.Println(err)
		}
		defer rows_table_like.Close()

		//fmt.Println(rows_table_like.Columns())
		var sfg_gallery string
		for rows_table_like.Next() {
			rows_table_like.Scan(&sfg_gallery)
		}
		if sfg_gallery != "" {
//========================================= get gallery information for current user ========================================//
			//execute query
			rows_client, err := db.Query("SELECT name, show_categories FROM sfg_gallery")
			if err != nil {
				fmt.Println("Could not get gallery parameters from user db " + sf_db + ".")
				fmt.Println(err)
			}
			defer rows_client.Close()
//========================================== output information about current user ==========================================//
			/*c.String(200,	"<div class='dropdown'>"+
							"<button class='btn btn-default dropdown-toggle' type='button' id='dropdownMenu_"+sf_db+"' data-toggle='dropdown' aria-expanded='true'>"+
								sf_db+
								"<span class='caret'></span>"+
							"</button>"+
							"<ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu_"+sf_db+"'>")*/ //dropdown
			//for each resulting row do stuff
			for rows_client.Next() {
				var name, show_categories string
				rows_client.Scan(&name, &show_categories)
				//give names to gallery types
/*				gallery_name := "Unknown"
				switch show_categories {
					case "0": {
						gallery_name = "Display"
					}
					case "1": {
						gallery_name = "Shop"
					}
					case "2": {
						gallery_name = "Category"
					}
					case "3": {
						gallery_name = "Carousel"
					}
					case "4": {
						gallery_name = "Coverflow"
					}
					case "5": {
						gallery_name = "Polaroid"
					}
					case "6": {
						gallery_name = "Wall"
					}
				}*/

				//fmt.Println(name + " " + show_categories + " " + gallery_name)
				//c.String(200, name + " " + show_categories + " " + gallery_name+"\n")

				//c.String(200, "<li role='presentation'><a role='menuitem' tabindex='-1' href='#'>"+name+" || "+gallery_name+"</a></li>") //dropdown
			}
			if err := rows_client.Err(); err != nil {
				fmt.Print(err)
				fmt.Println(" rows_client")
			}
			//c.String(200,"</ul></div>" ) //dropdown
		}
		progress_current++
		c.String(200, "<script>$('#progress_go').remove();</script>")
		c.String(200, "<div id='progress_go'>Progress:"+strconv.Itoa(progress_current)+"/"+strconv.Itoa(progress_full)+"</div>")
		/*progress_percent := progress_current/progress_full*100
		percent := 0
		if progress_percent == percent {
			c.String(200, "<script>$('#progress_go').remove();</script>")
			c.String(200, "<div id='progress_go' style='text-align:center; margin:0 auto;'>" +
						"<div class='progress'>" +
						"<div class='progress-bar info' role='progressbar' aria-valuenow='"+strconv.Itoa(progress_current)+"' aria-valuemin='0'aria-valuemax='"+strconv.Itoa(progress_full)+"' style='width: "+strconv.Itoa(progress_percent)+"%;'>"+strconv.Itoa(progress_percent)+"%</div></div></div>")
			percent++
		}*/
	}
	//set start time
	t = time.Now().Local()
	end_time := string(t.Format("15:04:05"))
	fmt.Println("\nend_time:",end_time)
	c.String(200, "\nend_time: "+end_time)
}
