package sellerfoxStatisticsPkg

import (
	"github.com/gin-gonic/gin"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
//	"log"
	"time"
//	"strconv"
	"strings"
)

func Get_users_and_galleries(databases_array []string, c *gin.Context)() {
	//set start time
	t := time.Now().Local()
	start_time := string(t.Format("15:04:05"))
	fmt.Println("start_time:",start_time)

	progress_full := len(databases_array)
	fmt.Println("progress_full:",progress_full)
	//progress_current := 0
//============================================== connect to sellerfox database ==============================================//
	//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
	db1, err := sql.Open("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/sellerfox?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect to sellerfox.", err)
		return
	} else {
		fmt.Println("Connected to db sellerfox.")
	}
	defer db1.Close()
	//================================================ connect to local database ================================================//
	//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")

	db2, err := sql.Open("mysql", "root:@tcp(localhost:3306)/sellerfox_statistics?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect to sellerfox statistics.", err)
		return
	} else {
		fmt.Println("Connected to db sellerfox statistics.")
	}
	defer db2.Close()
//========================================================== cycle! =========================================================//
	for _, sf_db := range databases_array {
//================================================= get client's sf_login_id ================================================//
		sf_id := strings.Replace(sf_db, "sellerfox_", "", -1)
//============================================ get client's contract information ============================================//
		//execute query
		rows_client_contract, err := db1.Query("SELECT sf_contract.sf_login_id, sf_contract.firma, sf_contract.name, sf_contract.vorname, sf_contract.land, sf_contract.tarif_id, sf_contract.gekuendiged, sf_contract.blocked, sf_contract.removed FROM sf_contract WHERE sf_contract.sf_login_id="+sf_id)
		if err != nil {
			fmt.Println("Could not get client's contract information. Client " + sf_db + ".")
			fmt.Println(err)
		} /*else if rows_client_contract != nil {
			rows_client_contract.Close()
		} else {*/
			defer rows_client_contract.Close()
		//}


		//fmt.Println(rows_client_contract.Columns())
		var sf_login_id, firma, name, vorname, land, tarif_id, gekuendiged, blocked, removed string
		for rows_client_contract.Next() {
			rows_client_contract.Scan(&sf_login_id, &firma, &name, &vorname, &land, &tarif_id, &gekuendiged, &blocked, &removed)
			fmt.Println(sf_login_id," ",firma," ", name," ", vorname," ", land," ", tarif_id," ", gekuendiged," ", blocked," ", removed)

//========================================== copy client's info into local database =========================================//
			//execute query
			rows_client_contract, err := db2.Query(	"INSERT INTO client (sf_login_id, firma, name, vorname, land, tarif_id, gekuendiged, blocked, removed) "+
													"VALUES ("+sf_login_id+", '"+firma+"', '"+name+"', '"+vorname+"', '"+land+"', "+tarif_id+", "+gekuendiged+", "+blocked+", "+removed+") "+
													"ON DUPLICATE KEY UPDATE "+
														"firma = '"+firma+"', "+
														"name = '"+name+"', "+
														"vorname = '"+vorname+"', "+
														"land = '"+land+"', "+
														"tarif_id = "+tarif_id+", "+
														"gekuendiged = "+gekuendiged+", "+
														"blocked = "+blocked+", "+
														"removed = "+removed)
			if err != nil {
				fmt.Println("Could not get client's contract information. Client " + sf_db + ".")
				fmt.Println(err)
			} /*else if rows_client_contract != nil {
				rows_client_contract.Close()
			} else {*/
				defer rows_client_contract.Close()
			//}
		}
	}










/*
//=========================================== if database is found, connect to it ===========================================//
	for _, sf_db := range databases_array {
		//connect to DB
		//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
		db, err := sql.Open("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/"+sf_db+"?charset=utf8") //"+sf_login_id+"
		if err != nil {
			fmt.Println("Failed to connect", err)
		} else {
			fmt.Println("\nConnected to db " + sf_db + ".")
		}
		defer db.Close()
//============================================== check if required table exists =============================================//
		//execute query
		rows_table_like, err := db.Query("SHOW TABLES LIKE 'sfg_gallery'")
		if err != nil {
			fmt.Println("Could not check if required tables exist user db " + sf_db + ".")
			fmt.Println(err)
		}
		defer rows_table_like.Close()

		//fmt.Println(rows_table_like.Columns())
		var sfg_gallery string
		for rows_table_like.Next() {
			rows_table_like.Scan(&sfg_gallery)
		}
		if sfg_gallery != "" {
//========================================= get gallery information for current user ========================================//
			//execute query
			rows_client, err := db.Query("SELECT name, show_categories FROM sfg_gallery")
			if err != nil {
				fmt.Println("Could not get gallery parameters from user db " + sf_db + ".")
				fmt.Println(err)
			}
			defer rows_client.Close()
//========================================== output information about current user ==========================================//
			//for each resulting row do stuff
			for rows_client.Next() {
				var name, show_categories string
				rows_client.Scan(&name, &show_categories)
				//give names to gallery types
				gallery_name := "Unknown"
				switch show_categories {
					case "0": {
						gallery_name = "Display"
					}
					case "1": {
						gallery_name = "Shop"
					}
					case "2": {
						gallery_name = "Category"
					}
					case "3": {
						gallery_name = "Carousel"
					}
					case "4": {
						gallery_name = "Coverflow"
					}
					case "5": {
						gallery_name = "Polaroid"
					}
					case "6": {
						gallery_name = "Wall"
					}
				}

				fmt.Println(name + " " + show_categories + " " + gallery_name)
			}
			if err := rows_client.Err(); err != nil {
				fmt.Print(err)
				fmt.Println(" rows_client")
			}
		}
		progress_current++
	}*/
	//set end time
	t = time.Now().Local()
	end_time := string(t.Format("15:04:05"))
	fmt.Println("\nend_time:",end_time)
}
