package sellerfoxStatisticsPkg

import (
//	"github.com/gin-gonic/gin"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"log"
	"time"
)

func Get_users_and_galleries_copy()() {
	//set start time
	t := time.Now().Local()
	start_time := string(t.Format("15:04:05"))
	fmt.Println("start_time:",start_time)
//============================================== connect to sellerfox database ==============================================//
	//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
	db, err := sql.Open("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/sellerfox?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect", err)
		return
	} else {
		fmt.Println("Connected to db sellerfox.")
	}
	defer db.Close()
//===================================== get a list of sellerfox users that were active ======================================//
	//execute query
	rows_ids, err := db.Query("SELECT DISTINCT sf_login_id FROM sfg_attach_gallery_queue")
	if err != nil {
		log.Panic(err)
		fmt.Println("Could not get list of sellerfox ids.")
	} else {
		fmt.Println("Got list of active users.")
	}
	defer rows_ids.Close()

	list_of_active_users := ""
//======================== for each user in list check if he has a database and get all his galleries =======================//
	//for each resulting row do stuff
	for rows_ids.Next() {
		var sf_login_id string
		rows_ids.Scan(&sf_login_id)
		//fmt.Println("Working with login_id:", sf_login_id)
//========================================== get a list of all databases on server ==========================================//
		//execute query
		rows_dbs, err := db.Query("SHOW DATABASES")
		if err != nil {
			fmt.Println(err)
		} else {
			//fmt.Println("Got list of databases.")
		}
		defer rows_dbs.Close()
//========================================= check if user has a database on server ==========================================//
		for rows_dbs.Next() {
			var Database string
			if err := rows_dbs.Scan(&Database); err != nil {
				fmt.Println(err)
			}
			//fmt.Print(Database) //delete later
			//fmt.Println(" sellerfox_"+sf_login_id) //delete later
			if "sellerfox_"+sf_login_id == Database {
//=========================================== if database is found, connect to it ===========================================//
				//fmt.Println("Database found for user:",sf_login_id) //delete later
				list_of_active_users += sf_login_id+" "
				//connect to DB
				//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
				db, err := sql.Open("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/sellerfox_"+sf_login_id+"?charset=utf8") //"+sf_login_id+"
				if err != nil {
					fmt.Println("Failed to connect", err)
				} else {
					fmt.Println("Connected to db sellerfox_" + sf_login_id + ".")
				}
				defer db.Close()
//========================================= get gallery information for current user ========================================//
				//execute query
				rows_client, err := db.Query("SELECT name, show_categories FROM sfg_gallery")
				if err != nil {
					fmt.Println("Could not get gallery parameters from user "+sf_login_id+".")
					fmt.Println(err)
				}
				defer rows_client.Close()
//========================================== output information about current user ==========================================//
				//fmt.Println("Client id:", sf_login_id)
				//for each resulting row do stuff
				for rows_client.Next() {
					var name, show_categories string
					rows_client.Scan(&name, &show_categories)
					//give names to gallery types
					gallery_name := "Unknown"
					switch show_categories {
						case "0": {
							gallery_name = "Display"
						}
						case "1": {
							gallery_name = "Shop"
						}
						case "2": {
							gallery_name = "Category"
						}
						case "3": {
							gallery_name = "Carousel"
						}
						case "4": {
							gallery_name = "Coverflow"
						}
						case "5": {
							gallery_name = "Polaroid"
						}
						case "6": {
							gallery_name = "Wall"
						}
					}

					fmt.Println(name+" "+show_categories+" "+gallery_name)
					//c.String(200, name+" "+show_categories+"\n")
				}
				if err := rows_client.Err(); err != nil {
					fmt.Print(err)
					fmt.Println(" rows_client")
				}
				fmt.Println("")

				//fmt.Println(sf_login_id)
				//c.String(200, sf_login_id+"\n")

				//get out of loop that checks if database exists for current user
				break
			} else {
				//fmt.Println("User with id:"+sf_login_id+" has no database.")
			}
		}
		if err := rows_dbs.Err(); err != nil {
			fmt.Print(err)
			fmt.Println(" row_dbs")
		}
	}
	if err := rows_ids.Err(); err != nil {
		fmt.Print(err)
		fmt.Println(" row_ids")
	}
	fmt.Println("\nList of active users:",list_of_active_users)
	//set start time
	t = time.Now().Local()
	end_time := string(t.Format("15:04:05"))
	fmt.Println("end_time:",end_time)
}
