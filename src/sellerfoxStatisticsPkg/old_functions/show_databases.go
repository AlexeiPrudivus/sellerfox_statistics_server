package sellerfoxStatisticsPkg

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"regexp"
	"github.com/jmoiron/sqlx"
)

type Databases struct {
	Database string
}

func Show_databases()([]string) {
	databases_list := ""
//============================================== connect to sellerfox database ==============================================//
	//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
	db, err := sqlx.Connect("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/sellerfox?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect to sellerfox.", err)
	} else {
		fmt.Println("Connected to db sellerfox.")
	}
	defer db.Close()

//============================================ get client's contract information ============================================//
	tx1 := db.MustBegin()
	//execute query
	rows := []Databases{}
	var e error
	e = tx1.Select(&rows,"SHOW DATABASES")
	tx1.Commit()
	if e != nil {
		fmt.Println("Could not get a list of databases.")
		fmt.Println(e)
	}
//========================================================== cycle! =========================================================//
	for _, row_client := range rows {
		databases_list += row_client.Database+" "
	}

	reg, _ := regexp.Compile("sellerfox_[0-9]{1,}")
	databases_array_reg := reg.FindAllStringSubmatch(databases_list, -1)
	//fmt.Printf("%#v\n", databases_array) //delete later

	databases_array := make([]string, len(databases_array_reg))
	for i, reg_1 := range databases_array_reg {
		for j, reg_2 := range reg_1 {
			if j == 0 {
				databases_array[i] = reg_2
			}
		}
	}
	//fmt.Println(databases_array)
	return databases_array
}
