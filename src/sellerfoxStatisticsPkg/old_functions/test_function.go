package sellerfoxStatisticsPkg

import (
	"github.com/gin-gonic/gin"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"log"
)

func Test_function(sellerfox_user_db string, c *gin.Context)() {
	fmt.Println("user id:",sellerfox_user_db)

	//connect to DB
	//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
	db, err := sql.Open("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/"+sellerfox_user_db+"?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect", err)
		return
	} else {
		fmt.Println("Connected to db.")
	}
	defer db.Close()

	//execute query
	rows, err := db.Query("SELECT name, show_categories FROM sfg_gallery")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	//for each resulting row do stuff
	for rows.Next() {
		var name, show_categories string
		if err := rows.Scan(&name, &show_categories); err != nil {
			log.Fatal(err)
		}
		fmt.Println(name+" "+show_categories)
		c.String(200, name+" "+show_categories+"\n")
	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}

	/*
	mysql_select_db($CONFIG['db'].'_'.add_zeros($row["sf_login_id"],5), $connection);
	$query = "SELECT show_categories FROM sfg_gallery WHERE id=".$row['gallery_id'];
	$sfg_gallery_result = mysql_query($query, $connection);
	$sfg_gallery_row = mysql_fetch_assoc($sfg_gallery_result);
	$show_categories = $sfg_gallery_row['show_categories'];
	*/
}
