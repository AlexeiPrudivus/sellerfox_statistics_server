package sellerfoxStatisticsPkg

import (
	"github.com/gin-gonic/gin"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"log"
)

func Get_gallery_types(rows_ids *sql.Rows, c *gin.Context)() {



	//for each resulting row do stuff
	for rows_ids.Next() {
		var sf_login_id string
		rows_ids.Scan(&sf_login_id)

		//connect to DB
		//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
		db, err := sql.Open("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/sellerfox_"+sf_login_id+"?charset=utf8")
		if err != nil {
			fmt.Println("Failed to connect", err)
			return
		} else {
			fmt.Println("Connected to db.")
		}
		defer db.Close()

		//execute query
		rows_client, err := db.Query("SELECT name, show_categories FROM sfg_gallery")
		if err != nil {
			log.Fatal(err)
		}
		defer rows_client.Close()

		fmt.Println("client id:", sf_login_id)
		//for each resulting row do stuff
		for rows_client.Next() {
			var name, show_categories string
			if err := rows_client.Scan(&name, &show_categories); err != nil {
				log.Fatal(err)
			}

			fmt.Println(name+" "+show_categories)
			//c.String(200, name+" "+show_categories+"\n")
		}
		if err := rows_client.Err(); err != nil {
			log.Fatal(err)
		}
		fmt.Println("")
	}
	if err := rows_ids.Err(); err != nil {
		fmt.Println(err)
	}
}
