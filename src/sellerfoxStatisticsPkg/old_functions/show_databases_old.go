package sellerfoxStatisticsPkg

import (
//	"github.com/gin-gonic/gin"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"log"
//	"strconv"
	"regexp"
)

func Show_databases()([]string) {
	databases_list := ""
	//connect to DB
	//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
	db, err := sql.Open("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/sellerfox?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect", err)
	} else {
		fmt.Println("Connected to db.")
	}
	defer db.Close()

	fmt.Println("Text after connection to db.") //delete later

	//execute query
	rows, err := db.Query("SHOW DATABASES")
	if err != nil {
		fmt.Println("Failed to execure query SHOW DATABASES.", err)
	} else {
		fmt.Println("Got list of databases on server.")
	}
	defer rows.Close()

	fmt.Println("Text after SHOW DATABASES.") //delete later

	for rows.Next() {
		var Database string
		if err := rows.Scan(&Database); err != nil {
			log.Fatal(err)
		}
		//fmt.Println(Database) //delete later
		databases_list += Database+" "
	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}

	reg, _ := regexp.Compile("sellerfox_[0-9]{1,}")
	databases_array_reg := reg.FindAllStringSubmatch(databases_list, -1)
	//fmt.Printf("%#v\n", databases_array) //delete later

	databases_array := make([]string, len(databases_array_reg))
	for i, reg_1 := range databases_array_reg {
		for j, reg_2 := range reg_1 {
			if j == 0 {
				databases_array[i] = reg_2
			}
		}
	}
	//fmt.Println(databases_array)
	return databases_array
}
