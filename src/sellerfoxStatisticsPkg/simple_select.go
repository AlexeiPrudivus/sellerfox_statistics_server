package sellerfoxStatisticsPkg

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"log"
)

func Simple_select() {
	//connect to DB
	//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
	//mysql://admin:KuL4nZg3htN1chT@ec2-46-137-76-114.eu-west-1.compute.amazonaws.com/sellerfox
	db, err := sql.Open("mysql", "statistics:statisticspass@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/sellerfox?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect", err)
	} else {
		fmt.Println("Connected to db.")
	}

	fmt.Println("Before SELECT")

	//execute query
	rows, err := db.Query("SELECT DISTINCT title FROM sf_tariff")
	if err != nil {
		fmt.Println("Error:", err)
	} else {
		fmt.Println("Got list of tarif titles on server.")
		fmt.Println("After SELECT")

		for rows.Next() {
			var Title string
			if err := rows.Scan(&Title); err != nil {
				log.Fatal(err)
			}
			fmt.Println(Title) //delete later
		}
		if err := rows.Err(); err != nil {
			log.Fatal(err)
		}
		defer rows.Close()
	}
	defer db.Close()
}
