package sellerfoxStatisticsPkg

import (
	"github.com/gin-gonic/gin"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"time"
	"strconv"
	"strings"
	"github.com/jmoiron/sqlx"
)

type R_client struct {
	Sf_login_id, Firma, Name, Vorname, Land, Gekuendiged, Blocked string
}

func Get_clients(databases_array []string, c *gin.Context) () {
	//set start time
	t := time.Now().Local()
	start_time := string(t.Format("15:04:05"))
	fmt.Println("start_time:", start_time)

	progress_full := len(databases_array)
	fmt.Println("progress_full:", progress_full)
	progress_current := 1
//============================================== connect to sellerfox database ==============================================//
	//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
	db1, err := sqlx.Connect("mysql", "admin:KuL4nZg3htN1chT@tcp(ec2-46-137-76-114.eu-west-1.compute.amazonaws.com:3306)/sellerfox?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect to sellerfox.", err)
		return
	} else {
		fmt.Println("Connected to db sellerfox.")
	}
	defer db1.Close()
//================================================ connect to local database ================================================//
	//sql.Open("mysql", "db_username:db_password@protocol(address:port_num)/database_name")
	db2, err := sqlx.Connect("mysql", "root:root-sellerfox@tcp(localhost:3306)/sellerfox_statistics?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect to sellerfox statistics.", err)
		return
	} else {
		fmt.Println("Connected to db sellerfox statistics.")
	}
	defer db2.Close()
//========================================================== cycle! =========================================================//
	for _, sf_db := range databases_array {
//================================================= get client's sf_login_id ================================================//
		sf_id := strings.Replace(sf_db, "sellerfox_", "", -1)
//============================================ get client's contract information ============================================//
		tx1 := db1.MustBegin()
		//execute query
		rows_client_contract := []R_client{}
		var e error
		e = tx1.Select(&rows_client_contract,"SELECT sf_contract.sf_login_id, sf_contract.firma, sf_contract.name, sf_contract.vorname, sf_contract.land, sf_contract.gekuendiged, sf_contract.blocked FROM sf_contract WHERE sf_contract.sf_login_id=" + sf_id)
		tx1.Commit()
		if e != nil {
			fmt.Println("Could not get client's contract information. Client " + sf_db + ".")
			fmt.Println(e)
		}
//========================================================== cycle! =========================================================//
		for _, row_client := range rows_client_contract {
			fmt.Println(row_client.Sf_login_id + ", '" + row_client.Firma + "', '" + row_client.Name + "', '" + row_client.Vorname + "', '" + row_client.Land + "', " + row_client.Gekuendiged + ", " + row_client.Blocked)
			//escape characters
			row_client.Firma = strings.Replace(row_client.Firma, "'", "&#39;", -1)  // '
			row_client.Firma = strings.Replace(row_client.Firma, "\"", "&#34;", -1) // "
//========================================== copy client's info into local database =========================================//
			tx2 := db2.MustBegin()
			//execute query
			_, err := tx2.Exec("INSERT INTO client (sf_login_id, firma, name, vorname, land, gekuendiged, blocked) " +
								"VALUES (" + row_client.Sf_login_id + ", '" + row_client.Firma + "', '" + row_client.Name + "', '" + row_client.Vorname + "', '" + row_client.Land + "', "  + row_client.Gekuendiged + ", " + row_client.Blocked + ") " +
								"ON DUPLICATE KEY UPDATE " +
								"firma = '" + row_client.Firma + "', " +
								"name = '" + row_client.Name + "', " +
								"vorname = '" + row_client.Vorname + "', " +
								"land = '" + row_client.Land + "', " +
								"gekuendiged = " + row_client.Gekuendiged + ", " +
								"blocked = " + row_client.Blocked )
			tx2.Commit()
			if err != nil {
				fmt.Println("Could not get client's contract information. Client " + sf_db + ".")
				fmt.Println(err)
			}
		}
//================================================== real-time progress bar =================================================//
		var progress_percent float32 = float32(progress_current) / float32(progress_full)* 100
		if progress_current == 1 || progress_current % (progress_full / 200) == 0 || progress_current == progress_full{
			progress_string	:= "<div id='script'>" +
					"<script>" +
					"document.getElementById('progress').remove();" +
					"document.getElementById('script').remove();" +
					"document.getElementById('filler').remove();" +
					"</script>" +
					"</div>" +
					"<div class='progress' style='margin: 0.5%;' id='progress'>" +
					"<div class='progress-bar' role='progressbar' aria-valuenow='"+strconv.Itoa(progress_current)+"' aria-valuemin='0' aria-valuemax='"+strconv.Itoa(progress_full)+"' style='width: "+strconv.FormatFloat(float64(progress_percent), 'f', 1, 32)+"%;'>" +
					"<span class='sr-only'>text</span>" +
					"</div>" +
					"</div>\n"
			dummy_symbols_count := 4096 - len(progress_string)
			dummy_string := "<div id='filler'>"
			for i :=0; i < dummy_symbols_count/20 - 23; i++ {
				dummy_string += "<!--/n/n/n/n/n/n/n/n/n/n/n/n/n/n/n/n/-->"
			}
			dummy_string += "</div>"
			c.String(200, progress_string+dummy_string)
		}
		progress_current++
	}
//======================================================== cycle over =======================================================//
	//set end time
	t = time.Now().Local()
	end_time := string(t.Format("15:04:05"))
	fmt.Println("\nend_time:", end_time)
}
