package sellerfoxStatisticsPkg

import (
	"github.com/jmoiron/sqlx"
	"fmt"
)

type Client_type_query struct {
	Total string
}

type Gallery_land_query struct {
	Count_land string
	Land string
}

type Gallery_tarif_query struct {
	Count_tarif_id string
	Tarif_id string
}

type Gallery_price_query struct {
	Count_price string
	Price string
}

type Client_cancelled_query struct {
	Total string
}

type Gallery_title_query struct {
	Count_title string
	Title string
}

func Get_stats_clients()(string, [][]string, string, string, [][]string, [][]string) {
//================================================ connect to local database ================================================//
	db, err := sqlx.Connect("mysql", "root:root-sellerfox@tcp(localhost:3306)/sellerfox_statistics?charset=utf8")
	if err != nil {
		fmt.Println("Failed to connect to sellerfox statistics.", err)
	} else {
		//fmt.Println("Connected to db sellerfox statistics.") //temp
	}
	defer db.Close()
//================================================ get number of all clients ================================================//
	row_clients_count := []Client_type_query{}
	tx := db.MustBegin()
	var e error
	e = tx.Select(&row_clients_count,"SELECT count(client.sf_login_id)as total FROM client")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get total amount of clients.")
		fmt.Println(e)
	}
	//fmt.Println("Total number of galleries:",row_clients_count[0].Total) //delete later
//=================================== pack query results about amount of clients an array ===================================//
	data_clients_total := row_clients_count[0].Total
//======================================= get number of clients in a certain country ========================================//
	row_client_land := []Gallery_land_query{}
	tx = db.MustBegin()
	e = tx.Select(&row_client_land,"SELECT count(land) as count_land, land FROM `client` GROUP BY land ORDER BY count_land DESC")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get clients' countries.")
		fmt.Println(e)
	}
//================================ pack query results about clients' countries into an array ================================//
	//create a new 2-dimensional array to output data
	data_array_land := make([][]string, len(row_client_land))
	for i := range data_array_land {
		data_array_land[i] = make([]string, 2)
	}
	//fill new array with data received from query
	for i := 0; i < len(row_client_land); i++ {
		data_array_land[i][0] = row_client_land[i].Count_land
		data_array_land[i][1] = row_client_land[i].Land
	}
	//fmt.Println(data_array_land) //delete later
//==================================== get number of clients with cancelled subscription ====================================//
	row_client_cancelled := []Client_cancelled_query{}
	tx = db.MustBegin()
	e = tx.Select(&row_client_cancelled,"SELECT count(gekuendiged) as total FROM `client` WHERE gekuendiged <> 0")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get number of clients with cancelled subscriptions.")
		fmt.Println(e)
	}
//=================================== pack query results about amount of clients an array ===================================//
	data_clients_cancelled := row_client_cancelled[0].Total
//========================================= get number of clients that were blocked =========================================//
	row_client_blocked := []Client_cancelled_query{}
	tx = db.MustBegin()
	e = tx.Select(&row_client_blocked,"SELECT count(blocked) as total FROM `client` WHERE blocked <> 0")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get number of blocked clients.")
		fmt.Println(e)
	}
//=================================== pack query results about amount of clients an array ===================================//
	data_clients_blocked := row_client_blocked[0].Total
//======================================= get number of clients with a free contract ========================================//
	row_client_price := []Gallery_price_query{}
	tx = db.MustBegin()
	e = tx.Select(&row_client_price,"SELECT count(price) as count_price, price FROM `contract` WHERE price = 0.00")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get clients' contract prices.")
		fmt.Println(e)
	}
//================================== pack query results about clients with a free contract ==================================//
	//create a new 2-dimensional array to output data
	data_array_price := make([][]string, 3)
	for i := range data_array_price {
		data_array_price[i] = make([]string, 2)
	}
	//fill new array with data received from query
	data_array_price[0][0] = row_client_price[0].Count_price
	data_array_price[0][1] = row_client_price[0].Price
	//fmt.Println(data_array_land) //delete later
//============================================ get average price of all contracts ===========================================//
	row_client_price = []Gallery_price_query{}
	tx = db.MustBegin()
	e = tx.Select(&row_client_price,"SELECT ROUND(AVG(price), 2) as count_price, count(sf_login_id) as price FROM `contract`") //WHERE price <> 0.00
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get clients' contract prices.")
		fmt.Println(e)
	}
//============================== pack query results about average contract price into an array ==============================//
	//fill new array with data received from query
	data_array_price[1][0] = row_client_price[0].Count_price
	data_array_price[1][1] = row_client_price[0].Price
	//fmt.Println(data_array_land) //delete later
//=================================== get number of clients with the cheapest subscription ==================================//
	row_client_price = []Gallery_price_query{}
	tx = db.MustBegin()
	e = tx.Select(&row_client_price,"SELECT price as count_price, count(sf_login_id) as price FROM `contract` WHERE price = 6.90") //WHERE price <> 0.00
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get clients' contract prices.")
		fmt.Println(e)
	}
//================== pack query results about number of clients witch cheapest subscription into an array ===================//
	//fill new array with data received from query
	data_array_price[2][0] = row_client_price[0].Count_price
	data_array_price[2][1] = row_client_price[0].Price
	//fmt.Println(data_array_land) //delete later
//==================================== get number of clients with certain contract types ====================================//
	row_client_title := []Gallery_title_query{}
	tx = db.MustBegin()
	e = tx.Select(&row_client_title,"SELECT count(title) as count_title, title FROM `contract` GROUP BY title ORDER BY count_title DESC")
	tx.Commit()
	if e != nil {
		fmt.Println("Could not get clients' type of contracts.")
		fmt.Println(e)
	}
//=================================== pack query results about contract types into array ====================================//
	//create a new 2-dimensional array to output data
	data_array_title := make([][]string, len(row_client_title))
	for i := range data_array_title {
		data_array_title[i] = make([]string, 2)
	}
	//fill new array with data received from query
	for i := 0; i < len(row_client_title); i++ {
		data_array_title[i][0] = row_client_title[i].Count_title
		data_array_title[i][1] = row_client_title[i].Title
	}
	//fmt.Println(data_array_land) //delete later

	return data_clients_total, data_array_land, data_clients_cancelled, data_clients_blocked, data_array_price, data_array_title
}
