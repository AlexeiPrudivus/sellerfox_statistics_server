function show_div_1() {
    document.getElementById('div_1').setAttribute('style', 'visibility: visible;');
    document.getElementById('div_2').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_3').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_4').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_5').setAttribute('style', 'visibility: hidden;');
}
function show_div_2() {
    document.getElementById('div_1').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_2').setAttribute('style', 'visibility: visible;');
    document.getElementById('div_3').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_4').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_5').setAttribute('style', 'visibility: hidden;');
}
function show_div_3() {
    document.getElementById('div_1').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_2').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_3').setAttribute('style', 'visibility: visible;');
    document.getElementById('div_4').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_5').setAttribute('style', 'visibility: hidden;');
}
function show_div_4() {
    document.getElementById('div_1').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_2').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_3').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_4').setAttribute('style', 'visibility: visible;');
    document.getElementById('div_5').setAttribute('style', 'visibility: hidden;');
}
function show_div_5() {
    document.getElementById('div_1').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_2').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_3').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_4').setAttribute('style', 'visibility: hidden;');
    document.getElementById('div_5').setAttribute('style', 'visibility: visible;');
}